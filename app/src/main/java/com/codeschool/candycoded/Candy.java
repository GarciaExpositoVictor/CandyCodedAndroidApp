package com.codeschool.candycoded;

/**
 * Created by vgarcia on 01/02/2018.
 */

public class Candy {
    public int id;
    public String name;
    public String image;
    public String price;
    public String description;
}
