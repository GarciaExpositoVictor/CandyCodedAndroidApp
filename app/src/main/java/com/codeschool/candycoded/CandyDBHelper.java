package com.codeschool.candycoded;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by vgarcia on 02/02/2018.
 */

public class CandyDBHelper extends SQLiteOpenHelper {
    public CandyDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, CandyContract.DB_NAME, null, CandyContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CandyContract.SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(CandyContract.SQL_DELETE_ENTRIES);

    }
}
