package com.codeschool.candycoded;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String candyName = null;
        String candyPrice = null;
        String candyImage = null;
        String candyDescription = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = DetailActivity.this.getIntent();
        if(intent.hasExtra("position")){
            int position = intent.getIntExtra("position", 0);
            CandyDBHelper candyDBHelper = new CandyDBHelper(this, CandyContract.DB_NAME, null, 1);
            SQLiteDatabase db = candyDBHelper.getWritableDatabase();
            Cursor cursor = db.rawQuery("SELECT * FROM candy", null);
            cursor.moveToPosition(position);
            int columnIndex = cursor.getColumnIndexOrThrow(CandyContract.CandyEntry.COLUMN_NAME_NAME);
            candyName = cursor.getString(columnIndex);
        }
        TextView textViewName = this.findViewById(R.id.text_view_name);
        textViewName.setText(candyName);
        if(intent.hasExtra("candy_price")){
            candyPrice = intent.getStringExtra("candy_price");
        }
        TextView textViewPrice = this.findViewById(R.id.text_view_price);
        textViewPrice.setText(candyPrice);
        if(intent.hasExtra("candy_image")){
            candyImage = intent.getStringExtra("candy_image");
        }
        ImageView imageView = (ImageView)this.findViewById(R.id.image_view_candy);
        Picasso.with(this).load(candyImage).into(imageView);
        if(intent.hasExtra("candy_description")){
            candyDescription = intent.getStringExtra("candy_description");
        }
        TextView textViewDesc = this.findViewById(R.id.text_view_desc);
        textViewDesc.setText(candyDescription);
        Log.d("DetailActivity", candyImage + ", " + candyPrice + ", " + candyDescription + ", " + candyName);


    }
}
